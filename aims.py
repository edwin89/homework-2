import numpy as np

print "Welcome to the AIMS module" 

def std(lst):
    """calculates standard deviation"""
    sm = 0
    mn = sum(lst)/float(len(lst))
    for i in range(len(lst)):
       sm += pow((lst[i]-mn),2)
    return (sm/len(lst))**0.5



def avg_range(file_name):
    result=[]
    new_result= []
    for file in file_name:
        data=open(file)
        for line in data:
            if (line.startswith('Range')):
                result= line.strip().split(':')
                new_result.append(float(result[1]))
       
      
    data.close()
    return (np.average(new_result))
   



