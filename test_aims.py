import aims
from nose.tools import assert_almost_equal

def test_positives():
	lst =[3,3]
	ab = aims.std(lst)
	exp = 0
	assert_almost_equal(ab, exp)

def test_negatives():
	lst =[-3,-5]
	ab = aims.std(lst)
	exp = 1.0
	assert_almost_equal(ab, exp)

def test_float():
	lst =[0.3,0.5]
	ab = aims.std(lst)
	exp = 0.1
	assert_almost_equal(ab, exp)


def test_negative_positive():
	lst =[-1,3]
	ab = aims.std(lst)
	exp = 2
	assert_almost_equal(ab, exp)


def test_name():
    filename=['data/bert/audioresult-00215','data/bert/audioresult-00239']
    obs=aims.avg_range(filename)
    exp=6.0
    assert obs==exp

def test_name1():
    filename=['data/bert/audioresult-00466','data/bert/audioresult-00532','data/bert/audioresult-00359']
    obs=aims.avg_range(filename)
    exp=6.0
    assert_almost_equal(obs,exp)

def test_name2():
    filename=['data/bert/audioresult-00330','data/bert/audioresult-00265','data/bert/audioresult-00521','data/bert/audioresult-00451']
    obs=aims.avg_range(filename)
    exp=6.5
    assert_almost_equal(obs,exp)
